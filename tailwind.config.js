module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    colors: {
      themeColors: {
        themeLight: '#ffffff',
        themeWarm: '#eae7dc',
        themeDark: '#2f4454',
        themContemporary: '#1a1a1d'
      },

      black: {
        black0: '#000000',
      },

      background: {
        'primary': 'var(--bg-background-primary)',
      },
      font: {
        'primary': 'var(--font-primary)',
        'secondary': 'var(--font-secondary)'
      },
      anchor: {
        'primary': 'var(--anchor-primary)',
      },
      button: {
        'primary': 'var(--button-primary)',
      }
    },

  },
  variants: {},
  plugins: []
};
